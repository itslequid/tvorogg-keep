import React, { Component } from 'react';
import PropTypes from 'prop-types';

import EditNote from '../EditNote';
import strings from '../../strings';
import { defaultNoteState } from '../../reducers/notes';

/**
 * @REACT_BP
 */
class TakeNote extends Component {
  constructor(props) {
    super(props);
    this.state = {
      takeNoteExpand: false,
    };
    this.takeNoteExpand = this.takeNoteExpand.bind(this);
    this.takeNoteRetract = this.takeNoteRetract.bind(this);
    this.renderTakeANoteButtons = this.renderTakeANoteButtons.bind(this);
  }

  takeNoteExpand() {
    this.setState(() => ({
      takeNoteExpand: true,
    }));
  }

  takeNoteRetract() {
    this.setState(() => ({
      takeNoteExpand: false,
    }));
  }

  renderTakeANoteButtons() {
    return [
    ];
  }

  render() {
    if (this.state.takeNoteExpand) {
      return (
        <div className="note-card note-card--take-note">
          <EditNote focusTextBox focusPosition={0} noteToEdit={defaultNoteState} onClose={this.takeNoteRetract} onDone={this.props.onDone} />
        </div>
      );
    } else {
      return (
        <div className="note-card note-card--take-note">
          <div className="note-card__container">
            <div
              contentEditable
              data-placeholder={strings.takeANote}
              role="textbox"
              tabIndex={0}
              onKeyDown={this.takeNoteExpand}
              onClick={this.takeNoteExpand}
              className="note-card__textbox"
            />
            {null}
          </div>
        </div>
      );
    }
  }
}

TakeNote.propTypes = {
  onDone: PropTypes.func.isRequired,
};

export default TakeNote;
