import React, { Component } from 'react';

import './styles.scss';
import Layout from './components/Layout/';
import EditPortal from './components/EditPortal';

/**
 * @REACT_BP
 */
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Layout />
        <EditPortal />
      </React.Fragment>
    );
  }
}

export default App;
